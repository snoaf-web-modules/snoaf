'use strict';

let path = require('path');
let core = require('core-libs');

let bootstrap = core.bootstrap;
let renderEngine = core.renderEngine;

let HomeRenderCtx   = require('./context/HomeRenderContext').factory;

class HomeRenderer {

    init() {
        this.pages = {
            '/': path.join('home.html')
        };

        return Promise.resolve();
    }

    getPage(req, res, next) {
        if (this.pages.hasOwnProperty(req.url))
        {
            let filePath = renderEngine.getPagePath('snoaf', this.pages[req.url]);
            res.render(filePath, HomeRenderCtx.init(req));
        }
        else
        {
            next();
        }
    }
}

let _singleton = new HomeRenderer();
bootstrap.register(__filename, _singleton, _singleton.init);
module.exports = _singleton;