'use strict';

let merge = require('deepmerge');
let core = require('core-libs');

let CoreContext = core.context;

class HomeRenderContextFactory {

    constructor() {}

    init(req) {
        let selectorCtx = new HomeRenderContextMixin(req);
        let merged = merge(req.ctx, selectorCtx);

        req.ctx = merged;
        return merged;
    }
}

class HomeRenderContextMixin extends CoreContext {

    constructor(req) {
        super(req);
    }
}

module.exports.factory = new HomeRenderContextFactory();
module.exports.mixin = HomeRenderContextMixin;