'use strict';

let core = require('core-libs');
let bootstrap = core.bootstrap;
let hitch = core.functions.hitch;
let CoreRouter = core.router;

let HomeRenderer    = require('../renderer/HomeRenderer');

class HomeRouter extends CoreRouter {
    init() {
        return super.init('snoaf')
            .then(() => {
                this.start();
                return Promise.resolve();
            });
    }

    start() {
        // Install routes
        this.app
            .get('/*', hitch(HomeRenderer, 'getPage'))
        ;

    }
}

let _singleton = new HomeRouter();
bootstrap.register(__filename, _singleton, _singleton.init);
module.exports = _singleton;