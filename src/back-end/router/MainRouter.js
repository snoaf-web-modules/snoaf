'use strict';

let express = require('express');

let core = require('core-libs');
let bootstrap = core.bootstrap;
let logger = core.loggerFactory.getLogger(__filename);
let CoreRouter = core.router;
let renderEngine = core.renderEngine;

let htmlRouter      = require('./HtmlRouter');

class SnoafMainRouter extends CoreRouter {
    init() {
        return super.init('snoaf')
            .then(() => {
                this.install();
                return Promise.resolve();
            })
            .catch((err) => {
                logger.error('Could not setup Router, err:', err);
                return Promise.reject(err);
            });
    }

    install() {
        this.app.engine('html', renderEngine.getHtmlEngine);
        this.app.engine('bundle', renderEngine.getBundleEngine);

        // Install routes (order important)
        this.app
            .use('/static/js',       express.static(renderEngine.getStaticPath('snoaf', 'js')))
            .use('/static/css',      express.static(renderEngine.getStaticPath('snoaf', 'css')))
            .use('/static/fonts',    express.static(renderEngine.getStaticPath('snoaf', 'fonts')))
            .use('/static/images',   express.static(renderEngine.getImagePath('snoaf')))
            .use('',                 express.static(renderEngine.getStaticImagePath('snoaf', 'favicons')))

            .use('/', htmlRouter.getRouter())
        ;
    }
}

let _singleton = new SnoafMainRouter();
bootstrap.register(__filename, _singleton, _singleton.init);
module.exports = _singleton;